package LMS.domains;

import java.util.Set;

import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Data
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long authorId;
    private String authorNr;
    private String authorName;

    // @OneToMany(cascade = CascadeType.ALL, mappedBy = "author", fetch = FetchType.LAZY)
    // private Set<Book> books;
    private String bookByAuthor;

    public Author() {
    }

    public long getAuthorId() {
        return authorId;
    }

    public String getAuthorNr() {
        return authorNr;
    }

    public void setAuthorNr(String authorNr) {
        this.authorNr = authorNr;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getBooks() {
        return bookByAuthor;
    }

    public void setBooks(String bookByAuthor) {
        this.bookByAuthor = bookByAuthor;
    }

    // @JsonManagedReference
    // public Set<Book> getBooks() {
    //     return books;
    // }

    // public void setBooks(Set<Book> books) {
    //     this.books = books;
    // }
}
