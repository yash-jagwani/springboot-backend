package LMS.controllers;

import java.util.List;

import LMS.domains.Author;
import LMS.repositories.AuthorRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class AuthorController {
    
    @Autowired
    AuthorRepository authorRepository;

    @GetMapping("/authors")
    List<Author> list() {
        return (List<Author>) authorRepository.findAll();
    }

    @PostMapping("/authors")
    Author newAuthor(@RequestBody Author newAuthor) {     
        return authorRepository.save(newAuthor);
    }

    @GetMapping("/authors/{authorId}")
    Author one(@PathVariable Long authorId) {

        return authorRepository.findById(authorId)
        .orElseThrow(() -> new AuthorNotFoundException(authorId));
    }

    @PutMapping("/authors/{authorId}")
    Author replaceAuthor(@RequestBody Author newAuthor, @PathVariable Long authorId) {

        return authorRepository.findById(authorId)
        .map(Author -> {
            Author.setAuthorNr(newAuthor.getAuthorNr());
            Author.setAuthorName(newAuthor.getAuthorName());
            Author.setBooks(newAuthor.getBooks());
            return authorRepository.save(Author);
        })
        .orElseGet(() -> {
            return authorRepository.save(newAuthor);
        });
    }

    @DeleteMapping("/authors/{authorId}")
    void deleteAuthor(@PathVariable Long authorId) {
        authorRepository.deleteById(authorId);
    }
}
