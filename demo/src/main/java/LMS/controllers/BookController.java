package LMS.controllers;

import java.util.List;

import LMS.domains.Book;
import LMS.repositories.BookRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;



@RestController
@CrossOrigin
public class BookController {

    @Autowired
    BookRepository bookRepository;

    @GetMapping("/books")
    List<Book> list() {
        return (List<Book>) bookRepository.findAll();
    }

    @PostMapping("/books")
    Book newBook(@RequestBody Book newBook) {     
        return bookRepository.save(newBook);
    }

    @GetMapping("/books/{bookId}")
    Book one(@PathVariable Long bookId) {

        return bookRepository.findById(bookId)
        .orElseThrow(() -> new BookNotFoundException(bookId));
    }

    @PutMapping("/books/{bookId}")
    Book replaceBook(@RequestBody Book newBook, @PathVariable Long bookId) {

        return bookRepository.findById(bookId)
        .map(Book -> {
            Book.setBookNr(newBook.getBookNr());
            Book.setBookName(newBook.getBookName());
            Book.setAuthor(newBook.getAuthor());
            Book.setGenre(newBook.getGenre());
            Book.setPrice(newBook.getPrice());
            Book.setAvailable(newBook.isAvailable());
            Book.setCustomer(newBook.getCustomer());
            return bookRepository.save(Book);
        })
        .orElseGet(() -> {
            return bookRepository.save(newBook);
        });
    }

    @DeleteMapping("/books/{bookId}")
    void deleteBook(@PathVariable Long bookId) {
        bookRepository.deleteById(bookId);
    }
}
