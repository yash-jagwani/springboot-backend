package LMS.controllers;

public class AuthorNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    AuthorNotFoundException(Long authorId) {
        super("Could not find Author " + authorId);
      }

}
